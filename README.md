**Documentation**

* https://medium.com/@d_date/how-to-build-audio-chat-video-chat-in-ios-using-webrtc-3c62e3563aff
* https://github.com/ISBX/apprtc-ios
* https://github.com/hiroeorz/AppRTCDemo
* https://github.com/stasel/WebRTC-iOS
* https://webrtc.googlesource.com/src/+/refs/heads/master/examples/objc/AppRTCMobile/

**Server NodeJS**

*npm install && node index.js*

**Flow Peer-to-peer stream**

*Repair
* Change valid stream link.
* Choose valid websocket link.

*Run
* Build and run target Peer1 and Peer2 on two difference devices.

* Peer1 goes to Sender Peer feature.
* Peer2 goes to Receiver Peer feature, press on Connect button to start peers connection.

* Peer1 plays.
* Peer2 plays.

* Peer1 loads ts segment from server, then caches it on disk.
* Peer2 just requests ts segement from Peer1, Peer1 loads ts cached, then sends to Peer2 via Peer-to-peer technical.

This project just is a small demo about P2P connecting, P2P exchange, P2P Streaming.
So, it still contains many issues such as: connect to serveral peers, solve thread-safe on receiving a bunch of part of segements, performance optimization.
