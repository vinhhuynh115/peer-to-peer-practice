//
//  Constants.swift
//  Peer1
//
//  Created by Vinh Huynh on 4/27/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

let serverScheme = "https"
//lazy var serverHost = "bitdash-a.akamaihd.net"
//lazy var serverHost = "livecdn.fptplay.net"
let serverHost = "s2-fpt-cdn.sohatv.vn"

let websocketURLStr = "ws://localhost:5000"
//let websocketURLStr = "https://sleepy-mesa-40346.herokuapp.com"

let streamURLStr = "https://s2-fpt-cdn.sohatv.vn/live/SWFYEZzBG7clIMM9/vtv/1000096/2020/04/09/JnTu3cwriGGFMFAv/337a54767a7233703637564336316a6df82b9d85fd1aba542e8e578c0924382b26fe5b99c97b87eabcc8b48b67a96f8dc70c10d5c68a28a3004ded21891d0bd619039c9132f0e8b252b6e1055ef8c4ea304f41334aa88965f1e69e7be6f3ce0fd71fd668f4c684d5321a987e2427af34ce44a5e02a063dbad342c0e63f4df1475cb8abe7b00d276019baec3bb01125572d748157106885538294552e56fdb6245f3dec71adf05dbbe8d1101deece1506/480.m3u8"
//let streamURLStr = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa_video_180_250000.m3u8"

#if PEER2
let peerId = "Peer2"
let localScheme = "http"
let localHost = "127.0.0.1"
let localPort: UInt = 8081
#else
let peerId = "Peer1"
let localScheme = "http"
let localHost = "127.0.0.1"
let localPort: UInt = 8080
#endif
