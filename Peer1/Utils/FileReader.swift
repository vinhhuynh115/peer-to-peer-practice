//
//  FileReader.swift
//  ReadFileInChunks
//
//  Created by Vinh Huynh on 4/1/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import Foundation

class FileReader {
    
    enum ReadFileEvent {
        case error(Error)
        case didReadAChunk(Data, Int)
        case completed
    }
    
    let path: String
    
    private let chunkSize = 1024 * 10 /// Bytes
    
    init(path: String) {
        self.path = path
    }
    
    /// Read chunks file with massive memory
    func readFileInChunks(queue: DispatchQueue = DispatchQueue.global(qos: .utility),
                          response: @escaping (ReadFileEvent) -> Void) {
        
        queue.async {
            guard FileManager.default.fileExists(atPath: self.path) else {
                response(.error(NSError(domain: "File doesn't exist", code: -1, userInfo: nil)))
                return
            }

            do {
                let url = URL(fileURLWithPath: self.path)

                /// Open file for reading
                let outputFileHandle = try FileHandle(forReadingFrom: url)

                /// Get file size
                guard let values = try? url.resourceValues(forKeys: Set([.fileSizeKey])),
                    let totalBytes = values.fileSize,
                    totalBytes > 0 else {

                        response(.error(NSError(domain: "Unknown file size", code: -1, userInfo: nil)))
                        return
                }

                /// Reading bytes
                var readingBytes = 0

                /// Get the first chunk
                var chunkData = outputFileHandle.readData(ofLength: self.chunkSize)
                while !(chunkData.isEmpty) {

                    readingBytes += chunkData.count
                    let progress = Int((Float(readingBytes)/Float(totalBytes))*100)
                    print("Reading progress: \(progress)%")

                    response(.didReadAChunk(chunkData, progress))

                    chunkData = outputFileHandle.readData(ofLength: self.chunkSize)
                }

                outputFileHandle.closeFile()
                response(.completed)
                print("Read file completed")

            } catch {
                response(.error(error))
            }
        }
    }
    
    /// Read chunks file without massive memory
    func readFileByDispatchIO(queue: DispatchQueue = DispatchQueue.main,
                              response: @escaping (ReadFileEvent) -> Void) {
        
        guard let dispatch = DispatchIO(type: .random,
                                        path: (path as NSString).utf8String!,
                                        oflag: 0,
                                        mode: mode_t(O_RDONLY),
                                        queue: DispatchQueue.main,
                                        cleanupHandler: { error in
                                            
                                            print("Error: \(error)")
        }) else { return }
        
        guard let values = try? URL(fileURLWithPath: self.path).resourceValues(forKeys: Set([.fileSizeKey])),
            let totalBytes = values.fileSize,
            totalBytes > 0 else {
                
                response(.error(NSError(domain: "Unknown file size", code: -1, userInfo: nil)))
                return
        }
        
        readFileBy(dispatch: dispatch,
                   offset: 0,
                   length: chunkSize,
                   totalBytes: totalBytes,
                   queue: queue,
                   response: response)
    }
    
    private func readFileBy(dispatch: DispatchIO,
                            offset: Int,
                            length chunkSize: Int,
                            totalBytes: Int,
                            queue: DispatchQueue,
                            response: @escaping (ReadFileEvent) -> Void) {
    
        guard offset < totalBytes else {
            response(.completed)
            print("Completed")
            return
        }
        
        dispatch.read(offset: off_t(offset), length: chunkSize, queue: queue) { (isSuccessed, dispatchData, error) in
            if isSuccessed {
                if let data = dispatchData as AnyObject as? Data {
                    
                    let nextOffset = offset + chunkSize
                    let progress = min(Int((Float(nextOffset)/Float(totalBytes))*100), 100)
                    print("Reading progress: \(progress)%")
                    response(.didReadAChunk(data, progress))
                    
                    self.readFileBy(dispatch: dispatch,
                                    offset: nextOffset,
                                    length: chunkSize,
                                    totalBytes: totalBytes,
                                    queue: queue,
                                    response: response)
                } else {
                    print("Unable to cast dispatch data")
                    response(.error(NSError(domain: "Unable to cast dispatch data", code: -1, userInfo: nil)))
                }
            } else {
                response(.error(NSError(domain: "Reading error", code: Int(error), userInfo: nil)))
            }
        }
    }
}
