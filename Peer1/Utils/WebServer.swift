//
//  WebServer.swift
//  CatchingAVPlayerRequest
//
//  Created by Vinh Huynh on 3/30/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import GCDWebServer

final class WebServer {
    static let shared = WebServer()
    
    private lazy var webServer = GCDWebServer()
    
    var resourceRequestLogger: ((URL) -> Void)?
    var resourceRequestHandler: ((_ streamReq: URL, _ response: @escaping (Data) -> Void) -> Void)?
    
    func start() {
        
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self) { (request, completion) in
            let response = GCDWebServerDataResponse(html: "<html><body><p>Hello World</p></body></html>")
            completion(response)
        }
        
        print("Home directory: \(NSHomeDirectory())")
        webServer.addGETHandler(forBasePath: "/",
                                directoryPath: NSHomeDirectory(),
                                indexFilename: nil,
                                cacheAge: 3600,
                                allowRangeRequests: true)
        
        webServer.addHandler(forMethod: "GET", pathRegex: "/*.m3u8", request: GCDWebServerRequest.self) { (request, completion) in

            var components = URLComponents(string: request.url.absoluteString)
            components?.scheme = serverScheme
            components?.host = serverHost
            components?.port = nil
            guard let serverSourceURL = components?.url else {
                return
            }
            print("Requesting \(request.url.absoluteString)")
            print("Redirecting \(serverSourceURL.absoluteString)")

            let request = URLRequest(url: components!.url!)
            let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    print(error)
                } else {
                    guard let data = data else {
                        print("Empty data")
                        return
                    }

                    let response = GCDWebServerDataResponse(data: data, contentType: "")
                    completion(response)
                }
            }
            dataTask.resume()
        }

//        webServer.addHandler(forMethod: "GET", pathRegex: "/*.ts", request: GCDWebServerRequest.self) { [weak self] (request, completion) in
//
//            guard let self = self else { return }
//
//            var components = URLComponents(string: request.url.absoluteString)
//            components?.scheme = self.serverScheme
//            components?.host = self.serverHost
//            components?.port = nil
//            guard let serverSourceURL = components?.url else {
//                return
//            }
//            print("Requesting \(request.url.absoluteString)")
//            print("Redirecting \(serverSourceURL.absoluteString)")
//
//            let response = GCDWebServerResponse(redirect: serverSourceURL, permanent: false)
//            completion(response)
//        }
        
        webServer.addHandler(forMethod: "GET", pathRegex: "/*.ts", request: GCDWebServerRequest.self) { [weak self] (request, completion) in
            
            guard let self = self else { return }
            
            var components = URLComponents(string: request.url.absoluteString)
            components?.scheme = serverScheme
            components?.host = serverHost
            components?.port = nil
            guard let serverSourceURL = components?.url else { return }
            
            self.resourceRequestLogger?(serverSourceURL)
            
            if let resourceRequestHandler = self.resourceRequestHandler {
                resourceRequestHandler(serverSourceURL) { data in
                    let response = GCDWebServerDataResponse(data: data, contentType: "")
                    completion(response)
                }
            } else {
                let response = GCDWebServerResponse(redirect: serverSourceURL, permanent: false)
                completion(response)
            }
        }
        
        webServer.start(withPort: localPort, bonjourName: nil)
        print("Listening on \(webServer.serverURL?.absoluteString ?? "")\n")
    }
    
    func stream(for url: URL) -> URL {
        var components = URLComponents(string: url.absoluteString)
        components?.scheme = localScheme
        components?.host = localHost
        components?.port = Int(localPort)
        return components?.url ?? url
    }
}
