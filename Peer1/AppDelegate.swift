//
//  AppDelegate.swift
//  Peer1
//
//  Created by Vinh Huynh on 3/31/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        RTCPeerConnectionFactory.initializeSSL()
        WebServer.shared.start()
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        RTCPeerConnectionFactory.deinitializeSSL()
    }
}

