//
//  ListTableViewController.swift
//  Peer1
//
//  Created by Vinh Huynh on 4/1/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import UIKit

class ListTableViewController: UITableViewController {
    
    struct Item {
        let title: String
        let segueId: String
    }
    
    lazy var items: [Item] = {[
        Item(
            title: "Data Channel Exchange Text",
            segueId: "segue.text.exchange"
        ),
        Item(
            title: "Data Channel Exchange File",
            segueId: "segue.file.exchange"
        ),
        Item(
            title: "Data Channel Exchange Stream",
            segueId: "segue.stream.exchange"
        ),
        Item(
            title: "Sender Peer",
            segueId: "segue.sender.peer"
        ),
        Item(
            title: "Receiver Peer",
            segueId: "segue.receiver.peer"
        )
    ]}()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = peerId
    }
    
    // MARK: UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath)
        cell.textLabel?.text = items[indexPath.row].title
        return cell
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: items[indexPath.row].segueId, sender: nil)
    }
}
