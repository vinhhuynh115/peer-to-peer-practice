//
//  ReceiverPeerViewController.swift
//  Peer1
//
//  Created by Vinh Huynh on 4/2/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import UIKit
import Starscream
import AVFoundation

class ReceiverPeerViewController: UIViewController {

    struct ChunkInfo {
        let chunkId: Int
        let fileId: String
        let chunkData: Data
    }
    
    // MARK: IBOutlet
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var bufferTextView: UITextView!
    @IBOutlet weak var loadingProgressTextView: UITextView!
    @IBOutlet weak var logTextView: UITextView!
    
    // MARK: Variables
    private lazy var player = AVPlayer()
    
    private lazy var remotePeerId = ""
    private var dataChannel: RTCDataChannel?
    private lazy var iceServers: [RTCICEServer] = []
    private lazy var factory = RTCPeerConnectionFactory()
    private lazy var defaultConstraints: RTCMediaConstraints? = {
        return RTCMediaConstraints(mandatoryConstraints: [], optionalConstraints: [])
    }()
    private lazy var peerConnection: RTCPeerConnection? = {
        let peerConnection = factory.peerConnection(withICEServers: iceServers, constraints: defaultConstraints, delegate: self)
        return peerConnection
    }()
    
    private lazy var chunkStorage: [String: [ChunkInfo]] = [:]
    private lazy var requestStorage: [String: (Data) -> Void] = [:]
    private lazy var requestBackupStorage: [String: URL] = [:]
    
    private var isConnectableToRemotePeer = false {
        willSet {
            
        }
    }
    lazy var socket: WebSocket = {
        let websocketURL = URL(string: websocketURLStr)!
        let socket = WebSocket(request: URLRequest(url: websocketURL))
        socket.delegate = self
        return socket
    }()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        
        self.isConnectableToRemotePeer = false
        connectWebsocket()
        
        setupPlayer()
        handleResourceRequest()
        handleResourceRequestLogger()
    }
    
    // MARK: IBAction
    @IBAction func onPlayPressed(_ sender: UIButton) {
        loadStream()
    }
    
    // MARK: Setup
    private func setupNavigationBar() {
        title = "Receiver \(peerId)"
        
        let connectButton = UIBarButtonItem(title: "Connect", style: .done, target: self, action: #selector(onConnectPressed))
        navigationItem.rightBarButtonItem = connectButton
    }
    
    @objc
    private func onConnectPressed() {
        log("Initial Peer Connection")
        log("Initial Data Channel")
        dataChannel = peerConnection?.createDataChannel(withLabel: "FileExchangeChannel", config: nil)
        dataChannel?.delegate = self
        peerConnection?.createOffer(with: self, constraints: defaultConstraints)
    }
}

// MARK: Player
extension ReceiverPeerViewController {
    private func setupPlayer() {
        playerView.playerLayer.player = player
        player.isMuted = true
    }
    
    private func loadStream() {
        let url = URL(string: streamURLStr )!
        let catchingURL = WebServer.shared.stream(for: url)
        let asset = AVURLAsset(url: catchingURL)
        let playerItem = AVPlayerItem(asset: asset)
        player.replaceCurrentItem(with: playerItem)
        player.play()
        //log("Loading stream \(url.absoluteString)")
        log("Loading stream \(url.absoluteString.components(separatedBy: "/").last!)")
    }
    
    private func handleResourceRequestLogger() {
        WebServer.shared.resourceRequestLogger = { [weak self] (requestURL) in
            //self?.logRequest("Requesting \(requestURL.path)")
            self?.logRequest("Requesting \(requestURL.path.components(separatedBy: "/").last!)")
        }
    }
    
    private func handleResourceRequest() {
        WebServer.shared.resourceRequestHandler = { [weak self] (requestURL, completion) in
            DispatchQueue.global(qos: .default).asyncAfter(deadline: .now() + 0.25) {
//                if self?.requestStorage[requestURL.path] == nil {
//                    self?.loadResourceFromPeer(requestURL.path)
//                }
//                self?.requestStorage[requestURL.path] = completion
//                self?.requestBackupStorage[requestURL.path] = requestURL
                
                let name = requestURL.path.components(separatedBy: "/").last!
                if self?.requestStorage[name] == nil {
                    self?.loadResourceFromPeer(name)
                }
                self?.requestStorage[name] = completion
                self?.requestBackupStorage[name] = requestURL
            }
        }
    }
    
    private func loadResourceFromPeer(_ fileId: String) {
        let messageParams: [String: Any] = [
            "message": "send_request_resource",
            "fileId": fileId,
        ]
        guard let sendingData = try? JSONSerialization.data(withJSONObject: messageParams, options: .prettyPrinted) else {
            log("Unable to stringify message parameters")
            return
        }
        self.dataChannel?.sendData(RTCDataBuffer(data: sendingData, isBinary: false))
        log("Requesting \(fileId)")
    }
}

// MARK: Utils
extension ReceiverPeerViewController {
    private func log(_ message: String) {
        print(message)
//        DispatchQueue.main.async {
//            self.logTextView.text += "\n"
//            self.logTextView.text += message
//            if self.logTextView.text.count > 0 {
//                let location = self.logTextView.text.count - 1
//                let bottom = NSMakeRange(location, 1)
//                self.logTextView.scrollRangeToVisible(bottom)
//            }
//        }
    }
    
    private func logRequest(_ message: String) {
        print(message)
        DispatchQueue.main.async {
            self.bufferTextView.text += "\n"
            self.bufferTextView.text += message
            if self.bufferTextView.text.count > 0 {
                let location = self.bufferTextView.text.count - 1
                let bottom = NSMakeRange(location, 1)
                self.bufferTextView.scrollRangeToVisible(bottom)
            }
        }
    }
    
    private func logLoadingProgress(_ message: String) {
        print(message)
        DispatchQueue.main.async {
            self.loadingProgressTextView.text += "\n"
            self.loadingProgressTextView.text += message
            if self.loadingProgressTextView.text.count > 0 {
                let location = self.loadingProgressTextView.text.count - 1
                let bottom = NSMakeRange(location, 1)
                self.loadingProgressTextView.scrollRangeToVisible(bottom)
            }
        }
    }
}

// MARK: Signaling Message
extension ReceiverPeerViewController {
    func receive(_ peers: [String]) {
        log("Receive Peers")
        guard peers.contains(where: { $0 != peerId }) else {
            self.isConnectableToRemotePeer = false
            return
        }
        self.isConnectableToRemotePeer = true
    }
    
    /*
     THE LOCAL PEER IS BEING OFFERED BY A REMOTE PEER
     A remote peer send an offer to the local peer
     This offer is used to the local peer setting remote description
     Then, the local peer create and send an answer to the remote peer
     */
    func receiveOffer(from remotePeerId: String, sessionDescription sdp: RTCSessionDescription) {
        log("Receive Offer")
        guard let peerConnection = peerConnection,
            peerConnection.signalingState == RTCSignalingStable else {
                print("Peer connection isn't stable")
                return
        }
        
        self.remotePeerId = remotePeerId
        self.peerConnection?.setRemoteDescriptionWith(self, sessionDescription: sdp)
        self.peerConnection?.createAnswer(with: self, constraints: defaultConstraints)
    }
    
    /*
     THE LOCAL PEER IS OFFERING TO A REMOTE PEER
     A remote peer send an answer to the local peer
     This answer is used to the local peer setting remote description
     Finishing Offer/Answer Step
     Next step: Send Ice Candidate
     */
    func receiveAnswer(from remotePeerId: String, sessionDescription sdp: RTCSessionDescription) {
        log("Receive Answer")
        guard let peerConnection = peerConnection,
            peerConnection.signalingState == RTCSignalingHaveLocalOffer else {
                print("Peer connection isn't have local offer")
                return
        }
        
        self.remotePeerId = remotePeerId
        self.peerConnection?.setRemoteDescriptionWith(self, sessionDescription: sdp)
    }
    
    /*
     A remote peer send an ice candidate to the local peer to determine how to connect two peers
     */
    func receive(_ iceCandidate: RTCICECandidate) {
        log("Receive ICE Candidate")
        peerConnection?.add(iceCandidate)
    }
}

// MARK: RTCDataChannelDelegate
extension ReceiverPeerViewController: RTCDataChannelDelegate {
    func channelDidChangeState(_ channel: RTCDataChannel!) {
        log("Data Channel: \(channel.state.rawValue)")
    }
    
    func channel(_ channel: RTCDataChannel!,
                 didReceiveMessageWith buffer: RTCDataBuffer!) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            self.log("Data channel received message")
            guard let messageParams = try? JSONSerialization.jsonObject(with: buffer.data, options: .allowFragments) as? [String: Any] else {
                self.log("Unable to serialization data to json")
                return
            }
            
            if let message = messageParams["message"] as? String {
                switch message {
                case "send_chunk_file":
                    if let fileId = messageParams["fileId"] as? String,
                        let chunkId = messageParams["chunkId"] as? Int,
                        let progress = messageParams["progress"] as? Int,
                        let chunkDataBased64 = messageParams["chunkData"] as? String,
                        let chunkData = Data(base64Encoded: chunkDataBased64) {
                        
                        //self.logLoadingProgress("Recevied \(fileId) \(progress)%")
                        
                        let chunk = ChunkInfo(chunkId: chunkId, fileId: fileId, chunkData: chunkData)
                        var chunks: [ChunkInfo] = []
                        if let _chunks = self.chunkStorage[fileId] {  chunks = _chunks }
                        chunks.append(chunk)
                        //DispatchQueue.global(qos: .default).async {
                            self.chunkStorage[fileId] = chunks
                            
                            if progress >= 100 {
                                self.logLoadingProgress("\(fileId) received \(chunks.count) chunks")
                                
                                let mutableData = NSMutableData()
                                chunks.forEach({ mutableData.append($0.chunkData) })
                                self.requestStorage[fileId]?(mutableData as Data)
                                
                                self.chunkStorage.removeValue(forKey: fileId)
                                self.requestStorage.removeValue(forKey: fileId)
                                self.requestBackupStorage.removeValue(forKey: fileId)
                            }
                        //}
                    }
                case "send_full_file":
                    if let fileId = messageParams["fileId"] as? String,
                        let totalChunk = messageParams["totalChunk"] as? Int,
                        let chunks = self.chunkStorage[fileId] {
                        
//                        self.logLoadingProgress("Total \(totalChunk) chunks - Received \(chunks.count) chunks")
//
//                        let mutableData = NSMutableData()
//                        chunks.forEach({ mutableData.append($0.chunkData) })
//                        self.chunkStorage.removeValue(forKey: fileId)
//
//                        self.requestStorage[fileId]?(mutableData as Data)
//                        self.requestStorage.removeValue(forKey: fileId)
//                        self.requestBackupStorage.removeValue(forKey: fileId)
                    }
                case "send_file_not_exist":
                    if let fileId = messageParams["fileId"] as? String,
                        let url = self.requestBackupStorage[fileId] {
                        //let data = (try? Data(contentsOf: url)) ?? Data()
                        let data = Data()
                        self.requestStorage[fileId]?(data)
                        self.requestStorage.removeValue(forKey: fileId)
                        self.requestBackupStorage.removeValue(forKey: fileId)
                        self.logLoadingProgress("Load from server \(fileId)")
                    }
                default:
                    break
                }
            }
        }
    }
}

// MARK: RTCSessionDescriptionDelegate
extension ReceiverPeerViewController: RTCSessionDescriptionDelegate {
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didCreateSessionDescription sdp: RTCSessionDescription!,
                        error: Error!) {
        self.peerConnection?.setLocalDescriptionWith(self, sessionDescription: sdp)
        if (sdp.type == "offer") {
            self.sendOffer(sdp)
        } else if (sdp.type == "answer") {
            self.sendAnswer(sdp)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didSetSessionDescriptionWithError error: Error!) {
        if let error = error {
            log("\(#function) \(error)")
        }
    }
}

// MARK: RTCPeerConnectionDelegate
extension ReceiverPeerViewController: RTCPeerConnectionDelegate {
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        signalingStateChanged stateChanged: RTCSignalingState) {
        log("Signaling: \(stateChanged.rawValue)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        addedStream stream: RTCMediaStream!) {
        print(#function)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        removedStream stream: RTCMediaStream!) {
        print(#function)
    }
    
    func peerConnection(onRenegotiationNeeded peerConnection: RTCPeerConnection!) {
        print(#function)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        iceConnectionChanged newState: RTCICEConnectionState) {
        log("ICE Connection: \(newState.rawValue)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        iceGatheringChanged newState: RTCICEGatheringState) {
        log("ICE Gathering: \(newState.rawValue)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        gotICECandidate candidate: RTCICECandidate!) {
        
        print(#function)
        sendIceCandidate(candidate)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didOpen dataChannel: RTCDataChannel!) {
        print(#function)
        self.dataChannel = dataChannel
        self.dataChannel?.delegate = self
    }
}

// MARK: Websocket
extension ReceiverPeerViewController {
    func connectWebsocket() {
        log("Initial Websocket")
        socket.connect()
    }
    
    func sendPeerJoin() {
        log("Send Peer Join")
        let messageParams = [
            "message": "peer_join",
            "peerId": peerId
        ]
        guard let message = messageParams.stringify() else {
            log("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendOffer(_ sdp: RTCSessionDescription) {
        log("Send Offer")
        let messageParams: [String: Any] = [
            "message": "peer_offer",
            "peerId": peerId,
            "sdp": [
                "type": sdp.type,
                "desc": sdp.description
            ]
        ]
        guard let message = messageParams.stringify() else {
            log("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendAnswer(_ sdp: RTCSessionDescription) {
        log("Send Answer")
        let messageParams: [String: Any] = [
            "message": "peer_answer",
            "peerId": peerId,
            "remotePeerId": remotePeerId,
            "sdp": [
                "type": sdp.type,
                "desc": sdp.description
            ]
        ]
        guard let message = messageParams.stringify() else {
            log("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendIceCandidate(_ iceCandidate: RTCICECandidate) {
        log("Send ICE Candidate")
        let messageParams: [String: Any] = [
            "message": "peer_ice_candidate",
            "peerId": peerId,
            "remotePeerId": remotePeerId,
            "iceCandidate": [
                "sdp": iceCandidate.sdp ?? "",
                "sdpMid": iceCandidate.sdpMid ?? "",
                "sdpMLineIndex": iceCandidate.sdpMLineIndex
            ]
        ]
        guard let message = messageParams.stringify() else {
            log("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
}

// MARK: WebSocketDelegate
extension ReceiverPeerViewController: WebSocketDelegate {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            log("Websocket connected. Header: \(headers)")
            sendPeerJoin()
        case .disconnected(let reason, let code):
            log("Websocket disconnected. Reason: \(reason). Code: \(code)")
        case .text(let message):
            handleReceive(message)
        case .binary(let data):
            log("Websocket received binary: \(data.count)")
        default:
            log(#function)
        }
    }
    
    func handleReceive(_ message: String) {
        log("Websocket received message: \(message)")
        guard let params = message.parse(),
            let messageType = params["message"] as? String else {
                return
        }
        
        switch messageType {
        case "update_peers":
            if let peers = params["peers"] as? [String], !peers.isEmpty {
                receive(peers)
            }
        case "update_offer":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let sdpDict = params["sdp"] as? [String: Any],
                let sdpType = sdpDict["type"] as? String,
                sdpType == "offer",
                let sdpDesc = sdpDict["desc"] as? String,
                !sdpDesc.isEmpty,
                let sdp = RTCSessionDescription(type: sdpType, sdp: sdpDesc) {
                receiveOffer(from: remotePeerId, sessionDescription: sdp)
            }
        case "update_answer":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let sdpDict = params["sdp"] as? [String: Any],
                let sdpType = sdpDict["type"] as? String,
                sdpType == "answer",
                let sdpDesc = sdpDict["desc"] as? String,
                !sdpDesc.isEmpty,
                let sdp = RTCSessionDescription(type: sdpType, sdp: sdpDesc) {
                receiveAnswer(from: remotePeerId, sessionDescription: sdp)
            }
        case "update_ice_candidate":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let iceCandidateDict = params["iceCandidate"] as? [String: Any],
                let sdp = iceCandidateDict["sdp"] as? String,
                let sdpMid = iceCandidateDict["sdpMid"] as? String,
                let sdpMLineIndex = iceCandidateDict["sdpMLineIndex"] as? Int,
                let iceCandidate = RTCICECandidate(mid: sdpMid, index: sdpMLineIndex, sdp: sdp) {
                receive(iceCandidate)
            }
        default:
            log("Unknown \(messageType)")
        }
    }
}

// MARK: Utils
fileprivate extension Dictionary {
    func stringify() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            guard let theJSONText = String(data: jsonData, encoding: .ascii) else {
                return nil
            }
            return theJSONText
        } catch {
            return nil
        }
    }
}

fileprivate extension String {
    func parse() -> [String: Any]? {
        guard let data = data(using: .utf8),
            let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                return nil
        }
        return dict
    }
}

