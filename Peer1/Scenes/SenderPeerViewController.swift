//
//  SenderPeerViewController.swift
//  Peer1
//
//  Created by Vinh Huynh on 3/31/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import UIKit
import Starscream
import AVFoundation

class SenderPeerViewController: UIViewController {
    
    struct ChunkInfo {
        let chunkId: Int
        let fileId: String
        let chunkData: Data
    }
    
    // MARK: IBOutlet
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var bufferTextView: UITextView!
    @IBOutlet weak var cacheTextView: UITextView!
    @IBOutlet weak var logTextView: UITextView!
    
    // MARK: Variables
    private lazy var player = AVPlayer()
    
    private lazy var remotePeerId = ""
    private var dataChannel: RTCDataChannel?
    private lazy var iceServers: [RTCICEServer] = []
    private lazy var factory = RTCPeerConnectionFactory()
    private lazy var defaultConstraints: RTCMediaConstraints? = {
        return RTCMediaConstraints(mandatoryConstraints: [], optionalConstraints: [])
    }()
    private lazy var peerConnection: RTCPeerConnection? = {
        let peerConnection = factory.peerConnection(withICEServers: iceServers, constraints: defaultConstraints, delegate: self)
        return peerConnection
    }()
    
    private lazy var chunkStorage: [String: [ChunkInfo]] = [:]
    private lazy var requestStorage: [String: (Data) -> Void] = [:]
    
    private var isConnectableToRemotePeer = false {
        willSet {
            
        }
    }
    lazy var socket: WebSocket = {
        let websocketURL = URL(string: websocketURLStr)!
        let socket = WebSocket(request: URLRequest(url: websocketURL))
        socket.delegate = self
        return socket
    }()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Sender \(peerId)"
        
        self.isConnectableToRemotePeer = false
        connectWebsocket()
        
        setupPlayer()
        handleResourceRequest()
        handleResourceRequestLogger()
    }
    
    // MARK: IBAction
    @IBAction func onPlayPressed(_ sender: UIButton) {
        cleanCacheFolder()
        loadStream()
    }
}

// MARK: Player
extension SenderPeerViewController {
    private func setupPlayer() {
        playerView.playerLayer.player = player
    }
    
    private func loadStream() {
        let url = URL(string: streamURLStr)!
        let catchingURL = WebServer.shared.stream(for: url)
        let asset = AVURLAsset(url: catchingURL)
        let playerItem = AVPlayerItem(asset: asset)
        player.replaceCurrentItem(with: playerItem)
        player.play()
        //log("Loading stream \(url.absoluteString)")
        log("Loading stream \(url.absoluteString.components(separatedBy: "/").last!)")
    }
    
    private func handleResourceRequestLogger() {
        WebServer.shared.resourceRequestLogger = { [weak self] (requestURL) in
            //self?.logRequest("Requesting \(requestURL.path)")
            self?.logRequest("Requesting \(requestURL.path.components(separatedBy: "/").last!)")
        }
    }
    
    private func handleResourceRequest() {
        WebServer.shared.resourceRequestHandler = { [weak self] (requestURL, completion) in
            DispatchQueue.global(qos: .utility).async {
                guard let data = try? Data(contentsOf: requestURL) else { return }
                self?.cache(url: requestURL, with: data)
                completion(data)
            }
        }
    }
    
    private func cleanCacheFolder() {
        let fileDirectory = NSHomeDirectory() + "/cache_res"
        do {
            try FileManager.default.removeItem(at: URL(fileURLWithPath: fileDirectory))
        } catch {
            log("\(error)")
        }
    }
    
    private func cache(url: URL, with data: Data) {
        let fileDirectory = NSHomeDirectory() + "/cache_res"
        if !FileManager.default.fileExists(atPath: fileDirectory, isDirectory: UnsafeMutablePointer<ObjCBool>.allocate(capacity: 1)) {
            do {
                try FileManager.default.createDirectory(at: URL(fileURLWithPath: fileDirectory),
                                                        withIntermediateDirectories: true, attributes: nil)
                logCache("\(fileDirectory) created")
            } catch {
                logCache("\(fileDirectory) \(error)")
            }
        }
        
        let components = url.path.components(separatedBy: "/")
        guard let shortedFileName = components.last else { return }
        //let fileName = components.joined(separator: "_")
        //let filePath = fileDirectory + "/" + fileName
        let filePath = fileDirectory + "/" + shortedFileName
        do {
            try data.write(to: URL(fileURLWithPath: filePath))
            logCache("\(shortedFileName) \(data.count) bytes")
        } catch {
            logCache("\n=====\n\(shortedFileName) \(error)\n=====\n")
        }
    }
}

// MARK: Utils
extension SenderPeerViewController {
    private func log(_ message: String) {
        print(message)
        DispatchQueue.main.async {
            self.logTextView.text += "\n"
            self.logTextView.text += message
            if self.logTextView.text.count > 0 {
                let location = self.logTextView.text.count - 1
                let bottom = NSMakeRange(location, 1)
                self.logTextView.scrollRangeToVisible(bottom)
            }
        }
    }
    
    private func logRequest(_ message: String) {
        print(message)
        DispatchQueue.main.async {
            self.bufferTextView.text += "\n"
            self.bufferTextView.text += message
            if self.bufferTextView.text.count > 0 {
                let location = self.bufferTextView.text.count - 1
                let bottom = NSMakeRange(location, 1)
                self.bufferTextView.scrollRangeToVisible(bottom)
            }
        }
    }
    
    private func logCache(_ message: String) {
        print(message)
        DispatchQueue.main.async {
            self.cacheTextView.text += "\n"
            self.cacheTextView.text += message
            if self.cacheTextView.text.count > 0 {
                let location = self.cacheTextView.text.count - 1
                let bottom = NSMakeRange(location, 1)
                self.cacheTextView.scrollRangeToVisible(bottom)
            }
        }
    }
}

// MARK: Signaling Message
extension SenderPeerViewController {
    func receive(_ peers: [String]) {
        log("Receive Peers")
        guard peers.contains(where: { $0 != peerId }) else {
            self.isConnectableToRemotePeer = false
            return
        }
        self.isConnectableToRemotePeer = true
    }
    
    /*
     THE LOCAL PEER IS BEING OFFERED BY A REMOTE PEER
     A remote peer send an offer to the local peer
     This offer is used to the local peer setting remote description
     Then, the local peer create and send an answer to the remote peer
     */
    func receiveOffer(from remotePeerId: String, sessionDescription sdp: RTCSessionDescription) {
        log("Receive Offer")
        guard let peerConnection = peerConnection,
            peerConnection.signalingState == RTCSignalingStable else {
                print("Peer connection isn't stable")
                return
        }
        
        self.remotePeerId = remotePeerId
        self.peerConnection?.setRemoteDescriptionWith(self, sessionDescription: sdp)
        self.peerConnection?.createAnswer(with: self, constraints: defaultConstraints)
    }
    
    /*
     THE LOCAL PEER IS OFFERING TO A REMOTE PEER
     A remote peer send an answer to the local peer
     This answer is used to the local peer setting remote description
     Finishing Offer/Answer Step
     Next step: Send Ice Candidate
     */
    func receiveAnswer(from remotePeerId: String, sessionDescription sdp: RTCSessionDescription) {
        log("Receive Answer")
        guard let peerConnection = peerConnection,
            peerConnection.signalingState == RTCSignalingHaveLocalOffer else {
                print("Peer connection isn't have local offer")
                return
        }
        
        self.remotePeerId = remotePeerId
        self.peerConnection?.setRemoteDescriptionWith(self, sessionDescription: sdp)
    }
    
    /*
     A remote peer send an ice candidate to the local peer to determine how to connect two peers
     */
    func receive(_ iceCandidate: RTCICECandidate) {
        log("Receive ICE Candidate")
        peerConnection?.add(iceCandidate)
    }
}

// MARK: RTCDataChannelDelegate
extension SenderPeerViewController: RTCDataChannelDelegate {
    func channelDidChangeState(_ channel: RTCDataChannel!) {
        log("Data Channel: \(channel.state.rawValue)")
    }
    
    func channel(_ channel: RTCDataChannel!,
                 didReceiveMessageWith buffer: RTCDataBuffer!) {
        log("Data channel received message")
        guard let messageParams = try? JSONSerialization.jsonObject(with: buffer.data, options: .allowFragments) as? [String: Any] else {
            log("Unable to serialization data to json")
            return
        }
        
        if let message = messageParams["message"] as? String {
            switch message {
            case "send_request_resource":
                if let fileId = messageParams["fileId"] as? String {
                    responseFile(fileId)
                }
            default:
                break
            }
        }
    }
    
    private func responseFile(_ fileId: String) {
        
        let components = fileId.components(separatedBy: "/")
        let fileDirectory = NSHomeDirectory() + "/cache_res"
        let fileName = components.joined(separator: "_")
        let filePath = fileDirectory + "/" + fileName
        
        //log("Response file \(fileId). At path \(filePath)")
        log("Response file \(fileId)")
        
        var chunkCounter = 0
        let fileReader = FileReader(path: filePath)
        fileReader.readFileInChunks { [weak self] event in
            switch event {
            case .error(let error):
                let messageParams: [String: Any] = [
                    "message": "send_file_not_exist",
                    "fileId": fileId,
                ]
                guard let sendingData = try? JSONSerialization.data(withJSONObject: messageParams, options: .prettyPrinted) else {
                    self?.log("Unable to stringify message parameters")
                    return
                }
                self?.dataChannel?.sendData(RTCDataBuffer(data: sendingData, isBinary: false))
                self?.log("Read file in chunks: \(error)")
            case .didReadAChunk(let chunkData, let progress):
                let messageParams: [String: Any] = [
                    "message": "send_chunk_file",
                    "fileId": fileId,
                    "chunkId": chunkCounter,
                    "progress": progress,
                    "chunkData": chunkData.base64EncodedString()
                ]
                guard let sendingData = try? JSONSerialization.data(withJSONObject: messageParams, options: .prettyPrinted) else {
                    self?.log("Unable to stringify message parameters")
                    return
                }
                self?.dataChannel?.sendData(RTCDataBuffer(data: sendingData, isBinary: false))
                chunkCounter += 1
                self?.log("Sending \(progress)%")
                
            case .completed:
                let messageParams: [String: Any] = [
                    "message": "send_full_file",
                    "fileId": fileId,
                    "totalChunk": chunkCounter,
                ]
                guard let sendingData = try? JSONSerialization.data(withJSONObject: messageParams, options: .prettyPrinted) else {
                    self?.log("Unable to stringify message parameters")
                    return
                }
                self?.dataChannel?.sendData(RTCDataBuffer(data: sendingData, isBinary: false))
                self?.log("Sending completed")
            }
        }
    }
}

// MARK: RTCSessionDescriptionDelegate
extension SenderPeerViewController: RTCSessionDescriptionDelegate {
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didCreateSessionDescription sdp: RTCSessionDescription!,
                        error: Error!) {
        self.peerConnection?.setLocalDescriptionWith(self, sessionDescription: sdp)
        if (sdp.type == "offer") {
            self.sendOffer(sdp)
        } else if (sdp.type == "answer") {
            self.sendAnswer(sdp)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didSetSessionDescriptionWithError error: Error!) {
        if let error = error {
            log("\(#function) \(error)")
        }
    }
}

// MARK: RTCPeerConnectionDelegate
extension SenderPeerViewController: RTCPeerConnectionDelegate {
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        signalingStateChanged stateChanged: RTCSignalingState) {
        log("Signaling: \(stateChanged.rawValue)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        addedStream stream: RTCMediaStream!) {
        print(#function)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        removedStream stream: RTCMediaStream!) {
        print(#function)
    }
    
    func peerConnection(onRenegotiationNeeded peerConnection: RTCPeerConnection!) {
        print(#function)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        iceConnectionChanged newState: RTCICEConnectionState) {
        log("ICE Connection: \(newState.rawValue)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        iceGatheringChanged newState: RTCICEGatheringState) {
        log("ICE Gathering: \(newState.rawValue)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        gotICECandidate candidate: RTCICECandidate!) {
        
        print(#function)
        sendIceCandidate(candidate)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didOpen dataChannel: RTCDataChannel!) {
        print(#function)
        self.dataChannel = dataChannel
        self.dataChannel?.delegate = self
    }
}

// MARK: Websocket
extension SenderPeerViewController {
    func connectWebsocket() {
        log("Initial Websocket")
        socket.connect()
    }
    
    func sendPeerJoin() {
        log("Send Peer Join")
        let messageParams = [
            "message": "peer_join",
            "peerId": peerId
        ]
        guard let message = messageParams.stringify() else {
            log("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendOffer(_ sdp: RTCSessionDescription) {
        log("Send Offer")
        let messageParams: [String: Any] = [
            "message": "peer_offer",
            "peerId": peerId,
            "sdp": [
                "type": sdp.type,
                "desc": sdp.description
            ]
        ]
        guard let message = messageParams.stringify() else {
            log("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendAnswer(_ sdp: RTCSessionDescription) {
        log("Send Answer")
        let messageParams: [String: Any] = [
            "message": "peer_answer",
            "peerId": peerId,
            "remotePeerId": remotePeerId,
            "sdp": [
                "type": sdp.type,
                "desc": sdp.description
            ]
        ]
        guard let message = messageParams.stringify() else {
            log("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendIceCandidate(_ iceCandidate: RTCICECandidate) {
        log("Send ICE Candidate")
        let messageParams: [String: Any] = [
            "message": "peer_ice_candidate",
            "peerId": peerId,
            "remotePeerId": remotePeerId,
            "iceCandidate": [
                "sdp": iceCandidate.sdp ?? "",
                "sdpMid": iceCandidate.sdpMid ?? "",
                "sdpMLineIndex": iceCandidate.sdpMLineIndex
            ]
        ]
        guard let message = messageParams.stringify() else {
            log("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
}

// MARK: WebSocketDelegate
extension SenderPeerViewController: WebSocketDelegate {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            log("Websocket connected. Header: \(headers)")
            sendPeerJoin()
        case .disconnected(let reason, let code):
            log("Websocket disconnected. Reason: \(reason). Code: \(code)")
        case .text(let message):
            handleReceive(message)
        case .binary(let data):
            log("Websocket received binary: \(data.count)")
        default:
            log(#function)
        }
    }
    
    func handleReceive(_ message: String) {
        log("Websocket received message: \(message)")
        guard let params = message.parse(),
            let messageType = params["message"] as? String else {
                return
        }
        
        switch messageType {
        case "update_peers":
            if let peers = params["peers"] as? [String], !peers.isEmpty {
                receive(peers)
            }
        case "update_offer":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let sdpDict = params["sdp"] as? [String: Any],
                let sdpType = sdpDict["type"] as? String,
                sdpType == "offer",
                let sdpDesc = sdpDict["desc"] as? String,
                !sdpDesc.isEmpty,
                let sdp = RTCSessionDescription(type: sdpType, sdp: sdpDesc) {
                receiveOffer(from: remotePeerId, sessionDescription: sdp)
            }
        case "update_answer":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let sdpDict = params["sdp"] as? [String: Any],
                let sdpType = sdpDict["type"] as? String,
                sdpType == "answer",
                let sdpDesc = sdpDict["desc"] as? String,
                !sdpDesc.isEmpty,
                let sdp = RTCSessionDescription(type: sdpType, sdp: sdpDesc) {
                receiveAnswer(from: remotePeerId, sessionDescription: sdp)
            }
        case "update_ice_candidate":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let iceCandidateDict = params["iceCandidate"] as? [String: Any],
                let sdp = iceCandidateDict["sdp"] as? String,
                let sdpMid = iceCandidateDict["sdpMid"] as? String,
                let sdpMLineIndex = iceCandidateDict["sdpMLineIndex"] as? Int,
                let iceCandidate = RTCICECandidate(mid: sdpMid, index: sdpMLineIndex, sdp: sdp) {
                receive(iceCandidate)
            }
        default:
            log("Unknown \(messageType)")
        }
    }
}

// MARK: Utils
fileprivate extension Dictionary {
    func stringify() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            guard let theJSONText = String(data: jsonData, encoding: .ascii) else {
                return nil
            }
            return theJSONText
        } catch {
            return nil
        }
    }
}

fileprivate extension String {
    func parse() -> [String: Any]? {
        guard let data = data(using: .utf8),
            let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                return nil
        }
        return dict
    }
}

