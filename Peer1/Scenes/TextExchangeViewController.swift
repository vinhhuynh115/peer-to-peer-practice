//
//  TextExchangeViewController.swift
//  Peer1
//
//  Created by Vinh Huynh on 3/31/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import UIKit
import Starscream

class TextExchangeViewController: UIViewController {
    
    // MARK: Outlet
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var sendDataButton: UIButton!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var signalingLabel: UILabel!
    @IBOutlet weak var iceConnectionLabel: UILabel!
    @IBOutlet weak var iceGatheringLabel: UILabel!
    @IBOutlet weak var dataChannelLabel: UILabel!
    
    // MARK: Variable
    
    lazy var socket: WebSocket = {
        let websocketURL = URL(string: websocketURLStr)!
        let socket = WebSocket(request: URLRequest(url: websocketURL))
        socket.delegate = self
        return socket
    }()
    
    private lazy var iceServers: [RTCICEServer] = {
        let turnServers = [
            [
                "url": "turn:numb.viagenie.ca",
                "credential": "muazkh",
                "username": "webrtc@live.com"
            ],
            [
                "url": "turn:192.158.29.39:3478?transport=udp",
                "credential": "JZEOEt2V3Qb0y27GRntt2u2PAYA=",
                "username": "28224511:1379330808"
            ],
            [
                "url": "turn:192.158.29.39:3478?transport=tcp",
                "credential": "JZEOEt2V3Qb0y27GRntt2u2PAYA=",
                "username": "28224511:1379330808"
            ],
            [
                "url": "turn:turn.bistri.com:80",
                "credential": "homeo",
                "username": "homeo"
            ],
            [
                "url": "turn:turn.anyfirewall.com:443?transport=tcp",
                "credential": "webrtc",
                "username": "webrtc"
            ]
        ]
        
        return turnServers.map({
            RTCICEServer(uri: URL(string: $0["url"]!),
                         username: $0["username"],
                         password: $0["credential"])
        })
    }()
    
    private lazy var factory = RTCPeerConnectionFactory()
    private var dataChannel: RTCDataChannel?
    
    private var isConnectableToRemotePeer = false {
        willSet {
            stateView.isHidden = !newValue
            connectButton.isHidden = !newValue
            sendDataButton.isHidden = !newValue
            messageTextView.isEditable = !newValue
        }
    }
    private lazy var defaultConstraints: RTCMediaConstraints? = {
        return RTCMediaConstraints(mandatoryConstraints: [], optionalConstraints: [])
    }()
    private lazy var peerConnection: RTCPeerConnection? = {
        let peerConnection = factory.peerConnection(withICEServers: iceServers, constraints: defaultConstraints, delegate: self)
        return peerConnection
    }()
    
    lazy var remotePeerId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = peerId
        
        signalingLabel.text = nil
        dataChannelLabel.text = nil
        iceGatheringLabel.text = nil
        iceConnectionLabel.text = nil
        
        self.isConnectableToRemotePeer = false
        connectWebsocket()
    }
    
    // MARK: Func
    func addMessage(_ message: String) {
        DispatchQueue.main.async {
            self.messageTextView.text += "\n"
            self.messageTextView.text += message
            if self.messageTextView.text.count > 0 {
                let location = self.messageTextView.text.count - 1
                let bottom = NSMakeRange(location, 1)
                self.messageTextView.scrollRangeToVisible(bottom)
            }
        }
    }
    
    // MARK: Actions
    @IBAction func onConnectPressed(_ sender: UIButton) {
        addMessage("Initial Peer Connection")
        addMessage("Initial Data Channel")
        dataChannel = peerConnection?.createDataChannel(withLabel: "TextExchangeChannel", config: nil)
        dataChannel?.delegate = self
        peerConnection?.createOffer(with: self, constraints: defaultConstraints)
    }
    
    @IBAction func onSendDataPressed(_ sender: UIButton) {
        let messageData = Data("Hi \(remotePeerId), i'm \(peerId)".utf8)
        dataChannel?.sendData(RTCDataBuffer(data: messageData, isBinary: false))
    }
}

// MARK: Peer
extension TextExchangeViewController {
    func receive(_ peers: [String]) {
        addMessage("Receive Peers")
        guard peers.contains(where: { $0 != peerId }) else {
            self.isConnectableToRemotePeer = false
            return
        }
        self.isConnectableToRemotePeer = true
    }
    
    /*
     THE LOCAL PEER IS BEING OFFERED BY A REMOTE PEER
     A remote peer send an offer to the local peer
     This offer is used to the local peer setting remote description
     Then, the local peer create and send an answer to the remote peer
     */
    func receiveOffer(from remotePeerId: String, sessionDescription sdp: RTCSessionDescription) {
        addMessage("Receive Offer")
        guard let peerConnection = peerConnection,
            peerConnection.signalingState == RTCSignalingStable else {
                print("Peer connection isn't stable")
                return
        }
        
        self.remotePeerId = remotePeerId
        self.peerConnection?.setRemoteDescriptionWith(self, sessionDescription: sdp)
        self.peerConnection?.createAnswer(with: self, constraints: defaultConstraints)
    }
    
    /*
     THE LOCAL PEER IS OFFERING TO A REMOTE PEER
     A remote peer send an answer to the local peer
     This answer is used to the local peer setting remote description
     Finishing Offer/Answer Step
     Next step: Send Ice Candidate
     */
    func receiveAnswer(from remotePeerId: String, sessionDescription sdp: RTCSessionDescription) {
        addMessage("Receive Answer")
        guard let peerConnection = peerConnection,
            peerConnection.signalingState == RTCSignalingHaveLocalOffer else {
                print("Peer connection isn't have local offer")
                return
        }
        
        self.remotePeerId = remotePeerId
        self.peerConnection?.setRemoteDescriptionWith(self, sessionDescription: sdp)
    }
    
    /*
     A remote peer send an ice candidate to the local peer to determine how to connect two peers
     */
    func receive(_ iceCandidate: RTCICECandidate) {
        addMessage("Receive ICE Candidate")
        peerConnection?.add(iceCandidate)
    }
}

// MARK: RTCDataChannelDelegate
extension TextExchangeViewController: RTCDataChannelDelegate {
    func channelDidChangeState(_ channel: RTCDataChannel!) {
        print(#function, channel.state.rawValue)
        DispatchQueue.main.async {
            self.dataChannelLabel.text = "Data Channel: \(channel.state.rawValue)"
        }
    }
    
    func channel(_ channel: RTCDataChannel!,
                 didReceiveMessageWith buffer: RTCDataBuffer!) {
        print(#function, buffer.data.count)
        if let message = String(data: buffer.data, encoding: .utf8) {
            print(message)
            addMessage(message)
        }
    }
}

// MARK: RTCSessionDescriptionDelegate
extension TextExchangeViewController: RTCSessionDescriptionDelegate {
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didCreateSessionDescription sdp: RTCSessionDescription!,
                        error: Error!) {
        print(#function)
        self.peerConnection?.setLocalDescriptionWith(self, sessionDescription: sdp)
        if (sdp.type == "offer") {
            self.sendOffer(sdp)
        } else if (sdp.type == "answer") {
            self.sendAnswer(sdp)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didSetSessionDescriptionWithError error: Error!) {
        print(#function)
        if let error = error {
            print(error)
        }
    }
}

// MARK: RTCPeerConnectionDelegate
extension TextExchangeViewController: RTCPeerConnectionDelegate {
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        signalingStateChanged stateChanged: RTCSignalingState) {
        print(#function, stateChanged.rawValue)
        DispatchQueue.main.async {
            self.signalingLabel.text = "Signaling: \(stateChanged.rawValue)"
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        addedStream stream: RTCMediaStream!) {
        print(#function)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        removedStream stream: RTCMediaStream!) {
        print(#function)
    }
    
    func peerConnection(onRenegotiationNeeded peerConnection: RTCPeerConnection!) {
        print(#function)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        iceConnectionChanged newState: RTCICEConnectionState) {
        print(#function, newState.rawValue)
        DispatchQueue.main.async {
            self.iceConnectionLabel.text = "ICE Connection: \(newState.rawValue)"
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        iceGatheringChanged newState: RTCICEGatheringState) {
        print(#function, newState.rawValue)
        DispatchQueue.main.async {
            self.iceGatheringLabel.text = "ICE Gathering: \(newState.rawValue)"
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        gotICECandidate candidate: RTCICECandidate!) {
        
        print(#function)
        sendIceCandidate(candidate)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection!,
                        didOpen dataChannel: RTCDataChannel!) {
        print(#function)
        self.dataChannel = dataChannel
        self.dataChannel?.delegate = self
    }
}

// MARK: Websocket
extension TextExchangeViewController {
    func connectWebsocket() {
        addMessage("Initial Websocket")
        socket.connect()
    }
    
    func sendPeerJoin() {
        addMessage("Send Peer Join")
        let messageParams = [
            "message": "peer_join",
            "peerId": peerId
        ]
        guard let message = messageParams.stringify() else {
            print("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendOffer(_ sdp: RTCSessionDescription) {
        addMessage("Send Offer")
        let messageParams: [String: Any] = [
            "message": "peer_offer",
            "peerId": peerId,
            "sdp": [
                "type": sdp.type,
                "desc": sdp.description
            ]
        ]
        guard let message = messageParams.stringify() else {
            print("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendAnswer(_ sdp: RTCSessionDescription) {
        addMessage("Send Answer")
        let messageParams: [String: Any] = [
            "message": "peer_answer",
            "peerId": peerId,
            "remotePeerId": remotePeerId,
            "sdp": [
                "type": sdp.type,
                "desc": sdp.description
            ]
        ]
        guard let message = messageParams.stringify() else {
            print("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
    
    func sendIceCandidate(_ iceCandidate: RTCICECandidate) {
        addMessage("Send ICE Candidate")
        let messageParams: [String: Any] = [
            "message": "peer_ice_candidate",
            "peerId": peerId,
            "remotePeerId": remotePeerId,
            "iceCandidate": [
                "sdp": iceCandidate.sdp ?? "",
                "sdpMid": iceCandidate.sdpMid ?? "",
                "sdpMLineIndex": iceCandidate.sdpMLineIndex
            ]
        ]
        guard let message = messageParams.stringify() else {
            print("Unable to stringify message parameters")
            return
        }
        socket.write(string: message)
    }
}

// MARK: WebSocketDelegate
extension TextExchangeViewController: WebSocketDelegate {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            print("Websocket connected. Header: \(headers)")
            sendPeerJoin()
        case .disconnected(let reason, let code):
            print("Websocket disconnected. Reason: \(reason). Code: \(code)")
        case .text(let message):
            handleReceive(message)
        case .binary(let data):
            print("Websocket received binary: \(data.count)")
        default:
            print(#function)
        }
    }
    
    func handleReceive(_ message: String) {
        print("Websocket received message: \(message)")
        guard let params = message.parse(),
            let messageType = params["message"] as? String else {
                return
        }
        
        switch messageType {
        case "update_peers":
            if let peers = params["peers"] as? [String], !peers.isEmpty {
                receive(peers)
            }
        case "update_offer":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let sdpDict = params["sdp"] as? [String: Any],
                let sdpType = sdpDict["type"] as? String,
                sdpType == "offer",
                let sdpDesc = sdpDict["desc"] as? String,
                !sdpDesc.isEmpty,
                let sdp = RTCSessionDescription(type: sdpType, sdp: sdpDesc) {
                receiveOffer(from: remotePeerId, sessionDescription: sdp)
            }
        case "update_answer":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let sdpDict = params["sdp"] as? [String: Any],
                let sdpType = sdpDict["type"] as? String,
                sdpType == "answer",
                let sdpDesc = sdpDict["desc"] as? String,
                !sdpDesc.isEmpty,
                let sdp = RTCSessionDescription(type: sdpType, sdp: sdpDesc) {
                receiveAnswer(from: remotePeerId, sessionDescription: sdp)
            }
        case "update_ice_candidate":
            if let remotePeerId = params["remotePeerId"] as? String,
                !remotePeerId.isEmpty,
                let iceCandidateDict = params["iceCandidate"] as? [String: Any],
                let sdp = iceCandidateDict["sdp"] as? String,
                let sdpMid = iceCandidateDict["sdpMid"] as? String,
                let sdpMLineIndex = iceCandidateDict["sdpMLineIndex"] as? Int,
                let iceCandidate = RTCICECandidate(mid: sdpMid, index: sdpMLineIndex, sdp: sdp) {
                receive(iceCandidate)
            }
        default:
            print("Unknown \(messageType)")
        }
    }
}

// MARK: Utils
fileprivate extension Dictionary {
    func stringify() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            guard let theJSONText = String(data: jsonData, encoding: .ascii) else {
                return nil
            }
            return theJSONText
        } catch {
            return nil
        }
    }
}

fileprivate extension String {
    func parse() -> [String: Any]? {
        guard let data = data(using: .utf8),
            let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                return nil
        }
        return dict
    }
}
