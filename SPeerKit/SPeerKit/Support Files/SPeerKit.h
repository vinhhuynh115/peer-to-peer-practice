//
//  SPeerKit.h
//  SPeerKit
//
//  Created by Vinh Huynh on 4/6/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SPeerKit.
FOUNDATION_EXPORT double SPeerKitVersionNumber;

//! Project version string for SPeerKit.
FOUNDATION_EXPORT const unsigned char SPeerKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SPeerKit/PublicHeader.h>


