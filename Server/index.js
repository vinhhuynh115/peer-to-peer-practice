const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000

/// {peerId: Socket}
var peerMappable = {}

const server = express()
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index'))
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))

var WebSocketServer = require('ws').Server,
  wss = new WebSocketServer({ server })

wss.on('connection', function (socket) {
  socket.on('message', function (message) {
    console.log('Received: %s', message)
    let params = JSON.parse(message)
    switch (params.message) {
      case 'peer_join':
        addPeer(params, socket)
        break
      case 'peer_offer':
        handlePeerOffer(params)
        break
      case 'peer_answer':
        handlePeerAnswer(params)
        break
      case 'peer_ice_candidate':
        handlePeerIceCandidate(params)
        break
      case 'peer_file_info':
        handleFileInfo(params)
        break
      default:
        console.log('Message unknown')
    }
  })

  socket.send('Hi client')

})

function addPeer(params, socket) {
  if (params.peerId !== undefined && params.peerId != '') {
    peerMappable[params.peerId] = socket
    console.log(`${params.peerId} added!`)

    /// Notify new peer added
    const peers = Object.keys(peerMappable)
    for (const peer in peerMappable) {
      let socket = peerMappable[peer]
      if (socket === undefined) continue
      socket.send(JSON.stringify({message: 'update_peers', peers}))
    }
  }
}

function handlePeerOffer(params) {
  const peerId = params.peerId
  if (peerId === undefined || peerId == '') return

  for (const randomPeerId in peerMappable) {
    if (randomPeerId == peerId) continue

    const socket = peerMappable[randomPeerId]
    if (socket === undefined) continue

    const sdp = params.sdp
    if (typeof sdp !== 'object') continue

    console.log(`${peerId} is offering to ${randomPeerId}`)
    socket.send(JSON.stringify({message: 'update_offer', remotePeerId: peerId, sdp}))
    break
  }
}

function handlePeerAnswer(params) {
  const peerId = params.peerId
  if (peerId === undefined || peerId == '') return

  const remotePeerId = params.remotePeerId
  if (remotePeerId === undefined || remotePeerId == '') return

  const socket = peerMappable[remotePeerId]
  if (socket === undefined) return

  const sdp = params.sdp
  if (typeof sdp !== 'object') return

  console.log(`${peerId} is answering for ${remotePeerId}`)
  socket.send(JSON.stringify({message: 'update_answer', remotePeerId: peerId, sdp}))
}

function handlePeerIceCandidate(params) {
  const peerId = params.peerId
  if (peerId === undefined || peerId == '') return

  const remotePeerId = params.remotePeerId
  if (remotePeerId === undefined || remotePeerId == '') return

  const socket = peerMappable[remotePeerId]
  if (socket === undefined) return

  const iceCandidate = params.iceCandidate
  if (typeof iceCandidate !== 'object') return

  console.log(`${peerId} is sending ice candiate to ${remotePeerId}`)
  socket.send(JSON.stringify({message: 'update_ice_candidate', remotePeerId: peerId, iceCandidate}))
}
